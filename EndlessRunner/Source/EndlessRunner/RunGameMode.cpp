// Fill out your copyright notice in the Description page of Project Settings.


#include "RunGameMode.h"
#include "RunCharacter.h"
#include "Tile.h"

ARunGameMode::ARunGameMode()
{
	PrimaryActorTick.bCanEverTick = true;
}

void ARunGameMode::BeginPlay()
{
	Super::BeginPlay();
	
	//Spawn Initial Tiles
	for (size_t i = 0; i < 5; i++)
	{
		SpawnTile();
	}
}

void ARunGameMode::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

//After everytile Exit, spawn a new one
void ARunGameMode::OnTileExit(ATile* tile)
{
	SpawnTile();

	//Destroy Tile after 0.7 seconds
	GetWorldTimerManager().SetTimer(timeDelay, FTimerDelegate::CreateUObject(this, &ARunGameMode::DestroyTile, tile), 0.7f, true);
}

void ARunGameMode::SpawnTile()
{
	FActorSpawnParameters SpawnInfo;
	ATile* tile = GetWorld()->SpawnActor<ATile>(Tile, next, SpawnInfo);

	//GetAttachedTransform -> see Tile script
	next = tile->GetAttachedTransform();
	tile->TileExit.AddDynamic(this, &ARunGameMode::OnTileExit);
}

void ARunGameMode::DestroyTile(ATile* tiles)
{
	tiles->Destroy();
}
