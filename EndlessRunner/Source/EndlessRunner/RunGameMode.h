// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "RunGameMode.generated.h"

/**
 * 
 */
UCLASS()
class ENDLESSRUNNER_API ARunGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ARunGameMode();

	virtual void Tick(float DeltaTime) override;

	FTimerHandle timeDelay;

	FTransform next;

private:
	UFUNCTION()
		void DestroyTile(ATile* tiles);

protected:
	virtual void BeginPlay() override;

	void SpawnTile();

	UFUNCTION()
		void OnTileExit(class ATile* tile);


	UPROPERTY(EditAnywhere, Category = "TileSpawn")
		TSubclassOf<class ATile> Tile;
	
};
