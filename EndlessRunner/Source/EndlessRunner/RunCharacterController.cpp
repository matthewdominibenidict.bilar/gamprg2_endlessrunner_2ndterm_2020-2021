// Fill out your copyright notice in the Description page of Project Settings.


#include "RunCharacterController.h"
#include "RunCharacter.h"
#include "Components/InputComponent.h"

ARunCharacterController::ARunCharacterController()
{

}

void ARunCharacterController::BeginPlay()
{
	Super::BeginPlay();
	runCharacter = Cast<ARunCharacter>(GetPawn());
}

void ARunCharacterController::MoveForward(float scale)
{
	runCharacter->AddMovementInput(runCharacter->GetActorForwardVector(), 1);
}

void ARunCharacterController::MoveRight(float scale)
{
	runCharacter->AddMovementInput(runCharacter->GetActorRightVector() * scale);
}

void ARunCharacterController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ARunCharacterController::SetupInputComponent()
{
	Super::SetupInputComponent();
	InputComponent->BindAxis("MoveForward", this, &ARunCharacterController::MoveForward);
	InputComponent->BindAxis("MoveRight", this, &ARunCharacterController::MoveRight);
}

//When Character Dies
void ARunCharacterController::Disable()
{
	DisableInput(Cast<ARunCharacterController>(this));
}

