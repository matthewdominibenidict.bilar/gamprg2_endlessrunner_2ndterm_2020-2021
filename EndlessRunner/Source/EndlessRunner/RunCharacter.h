// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "RunCharacter.generated.h"

UDELEGATE(BlueprintAuthorityOnly)
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FRespawn, class ARunCharacter*, OnDeath);

UCLASS()
class ENDLESSRUNNER_API ARunCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ARunCharacter();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	bool isDead;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class UCameraComponent* Camera;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class USpringArmComponent* CameraArm;

	UFUNCTION(BlueprintCallable, Category = "Die")
		void Die();

	UPROPERTY(BlueprintAssignable, Category = "Event Dispatcher")
		FRespawn OnDeath;

	UFUNCTION(BlueprintCallable, Category = "Add Coin")
		void AddCoin();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Coin")
		int32 Coins;
};
