// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "RunCharacterController.generated.h"

/**
 * 
 */
UCLASS()
class ENDLESSRUNNER_API ARunCharacterController : public APlayerController
{
	GENERATED_BODY()

public:
	ARunCharacterController();

	virtual void Tick(float DeltaTime) override;
	virtual void SetupInputComponent() override;
	FVector CurrentVelocity;
	void Disable();

private:
	class ARunCharacter* runCharacter;

protected:
	virtual void BeginPlay() override;
	void MoveForward(float scale);
	void MoveRight(float scale);
};
