// Fill out your copyright notice in the Description page of Project Settings.


#include "Tile.h"
#include "RunCharacter.h"
#include "Obstacle.h"
#include "Pickup.h"
#include "Components/SceneComponent.h"
#include "Components/BoxComponent.h"
#include "Components/ArrowComponent.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
ATile::ATile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>("SceneComponent");
	SetRootComponent(SceneComponent);

	ExitTrigger = CreateDefaultSubobject<UBoxComponent>("ExitTrigger");
	ExitTrigger->SetupAttachment(SceneComponent);

	ArrowPoint = CreateDefaultSubobject<UArrowComponent>("ArrowPoint");
	ArrowPoint->SetupAttachment(SceneComponent);

	BoundingBox = CreateDefaultSubobject<UBoxComponent>("BoundingBox");
	BoundingBox->SetupAttachment(SceneComponent);

	PickupBoxArea = CreateDefaultSubobject<UBoxComponent>("PickupBoxArea");
	PickupBoxArea->SetupAttachment(SceneComponent);
}

// Called when the game starts or when spawned
void ATile::BeginPlay()
{
	Super::BeginPlay();

	//Connecting Delegate
	ExitTrigger->OnComponentBeginOverlap.AddDynamic(this, &ATile::OnOverlapBegin);
	WhichToSpawn();
}

// Called every frame
void ATile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

//Getting the next position of the arrow, where the tile will spawn next
FTransform ATile::GetAttachedTransform()
{
	FTransform attachPoint;
	attachPoint = ArrowPoint->GetComponentTransform();
	return attachPoint;
}

//Getting a random location within a set area
FVector ATile::GetRandomPointInBoundingBox(UBoxComponent* WhichBox)
{
	FVector Origin = WhichBox->Bounds.Origin;
	FVector BoxExtent = WhichBox->Bounds.BoxExtent;
	return UKismetMathLibrary::RandomPointInBoundingBox(Origin, BoxExtent);
}

//Setting the location, rotation, and scale 
FTransform ATile::GetBox(UBoxComponent* ChosenBox)
{
	FVector loc = GetRandomPointInBoundingBox(ChosenBox);
	FRotator rot = GetRandomPointInBoundingBox(ChosenBox).Rotation();
	FVector scale = FVector(1.0, 1.0, 1.0);
	return UKismetMathLibrary::MakeTransform(loc, rot, scale);
}

void ATile::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (ARunCharacter* character = Cast<ARunCharacter>(OtherActor))
	{
		//Calling Delegate
		TileExit.Broadcast(this);
	}
}

void ATile::SpawnObstacle()
{
	FActorSpawnParameters SpawnInfo;
	int32 RanObstacle = FMath::RandRange(RanObsMin, RanObsMax);
	AObstacle* obs = GetWorld()->SpawnActor<AObstacle>(ObstacleTypes[RanObstacle], GetBox(BoundingBox), SpawnInfo);
	obs->AttachToActor(this, FAttachmentTransformRules::KeepWorldTransform);
	OSpawns.Add(obs);
}

void ATile::SpawnPickup()
{
	FActorSpawnParameters SpawnInfo;
	int32 RanPickup = FMath::RandRange(RanPicksMin, RanPicksMax);
	APickup* picks = GetWorld()->SpawnActor<APickup>(PickupTypes[RanPickup], GetBox(PickupBoxArea), SpawnInfo);
	picks->AttachToActor(this, FAttachmentTransformRules::KeepWorldTransform);
	OSpawns.Add(picks);
}

void ATile::WhichToSpawn()
{
	int32 randNum = FMath::RandRange(1, 100);

	if (randNum >= 1 && randNum < 50)
	{
		SpawnObstacle();
	}
	else if (randNum >= 50 && randNum <= 100)
	{
		for (size_t i = 0; i < 4; i++)
		{
			SpawnPickup();
		}
	}
}

void ATile::Destroyed()
{
	for (AActor* spawnObject : OSpawns)
	{
		spawnObject->Destroy();
	}

	for (AActor* spawnPickup : PSpawns)
	{
		spawnPickup->Destroy();
	}
}

