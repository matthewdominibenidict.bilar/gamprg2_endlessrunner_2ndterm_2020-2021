// Fill out your copyright notice in the Description page of Project Settings.


#include "RunCharacter.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "RunCharacterController.h"


// Sets default values
ARunCharacter::ARunCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CameraArm = CreateDefaultSubobject<USpringArmComponent>("CameraArm");
	CameraArm->SetupAttachment(GetMesh());
	CameraArm->TargetArmLength = 500.0f;

	Camera = CreateDefaultSubobject<UCameraComponent>("Camera");
	Camera->SetupAttachment(CameraArm);
}

// Called when the game starts or when spawned
void ARunCharacter::BeginPlay()
{
	Super::BeginPlay();

	//Initial Status of Player
	//Status After every Revive
	isDead = false;
	Coins = 0;
}

// Called every frame
void ARunCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ARunCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void ARunCharacter::Die()
{
	//isDead == True -> determined if character hit obstacle or not (see Obstacle script)
	if (isDead == true)
	{
		Cast<ARunCharacterController>(GetController())->Disable();
		GetMesh()->SetVisibility(false);
		OnDeath.Broadcast(this);
	}
}

//When character overlaps with coin, add 1 to score (see Pickup script and blueprint)
void ARunCharacter::AddCoin()
{
	Coins += 1;
}

